# Imagen base de Java
FROM debian:bullseye-slim

# Actualizar lista de paquetes
RUN apt-get update

# Instalar wget
RUN apt-get install wget -y

# Descargar e instalar Java 16
RUN mkdir -p /java/    
RUN wget "https://download.java.net/java/GA/jdk16.0.2/d4a915d82b4c4fbb9bde534da945d746/7/GPL/openjdk-16.0.2_linux-x64_bin.tar.gz" -P /java/
RUN tar -xzvf /java/openjdk-16.0.2_linux-x64_bin.tar.gz -C /java/
RUN rm -rf /java/openjdk-16.0.2_linux-x64_bin.tar.gz

# Mostrar que la version existe
RUN du -h /java

# Descargar el JAR del servidor
RUN mkdir -p /minecraft/
RUN wget "https://launcher.mojang.com/v1/objects/a16d67e5807f57fc4e550299cf20226194497dc2/server.jar" -P /minecraft/

# Aceptar las politicas de uso
RUN echo eula=true > /minecraft/eula.txt 

# Punto principal de arranque
CMD [ "/java/jdk-16.0.2/bin/java", "-jar", "/minecraft/server.jar", "nogui" ]

